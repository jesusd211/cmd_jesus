package terminal;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jesus
 */
public class Comandos {
   
    
    private java.nio.file.Path ruta;
    
    public String[] help() {
       //help-> Lista los comandos con una breve definición de lo que hacen. 
       String[] lista = new String[13]; 
       
       lista[0]="cd: \n"+
"  -> Muestra el directorio actual.  \n" +
"  -> [..] Accede al directorio padre.\n" +
"  -> [<nombreDirectorio>] Accede a un directorio dentro del directorio actual. \n" +
"  -> [<rutaAbsoluta] Accede a la ruta absoluta del sistema. \n";
       lista[1]="mkdir: <nombre_directorio>-> Crea un directorio en la ruta actual.\n";
       lista[2]="info: <nombre>-> Muestra la información del elemento.\n" +
"        Indicando FileSystem, Parent, Root, Nº of elements, FreeSpace, TotalSpace y UsableSpace.\n";
       lista[3]="cat  <nombreFichero>-> Muestra el contenido de un fichero. \n";
       lista[4]="top <numeroLineas> <nombreFichero>-> Muestra las líneas especificadas de un fichero. \n";
       lista[5]="mkfile <nombreFichero> <texto>-> Crea un fichero con ese nombre y el contenido de texto. \n";
       lista[6]="write <nombreFichero> <texto>-> Añade 'texto' al final del fichero especificado.\n";
       lista[7]="dir:\n"
               + "-> Lista los archivos o directorios de la ruta actual.\n"
               + "[<nombreDirectorio>]-> Lista los archivos o directorios dentro de ese directorio.\n"
               + "[<rutaAbsoluta]-> Lista los archivos o directorios dentro de esa ruta.\n";
       lista[8]="delete <nombre>-> Borra el fichero, si es un directorio borra todo su contenido y a si mismo.\n";
       lista[9]="close -> Cierra el programa.\n";
       lista[10]="Clear -> vacía la vista.";
       
       for (int i=0;i<lista.length;i++){
     
  }
      return lista;    
    }
        
    public String mkdir(String NombreDr) throws IOException {

        return null;

}
    public String info() {
        //info <nombre>-> Muestra la información del elemento.
        //Indicando FileSystem, Parent, Root, Nº of elements, FreeSpace, TotalSpace y UsableSpace.
        
        return null;
    
    
}
    public String cat() {
        //cat <nombreFichero>-> Muestra el contenido de un fichero.
        
        return null;
    
    
}
    public String top() {
        //top <numeroLineas> <nombreFichero>-> Muestra las líneas especificadas de un fichero 
        
        return null;
    
    
}
    public String mkfile() {
        //mkfile <nombreFichero> <texto>-> Crea un fichero con ese nombre y el contenido de texto.
        
        return null;
    
    
}public String write() {
        //write <nombreFichero> <texto>-> Añade 'texto' al final del fichero
        
        return null;
    
    
}public String dir() {
        /*dir-> Lista los archivos o directorios de la ruta actual.[<nombreDirectorio>]
          -> Lista los archivos o directorios dentro de ese directorio.[<rutaAbsoluta]
          -> Lista los archivos o directorios dentro de esa ruta.
        */

        return null;

}public String delete() {
       
        return null;
    
    
}public String close() {
        //close-> Cierra el programa.
        System.exit(0);
        return null;
    
    
}public String Clear() {
        //Clear -> vacía la vista.
        return null;
    
    
}

    public Path getRuta() {
        return ruta;
    }

    public void setRuta(Path ruta) {
        this.ruta = ruta;
    }

    
}

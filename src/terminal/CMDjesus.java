package terminal;

import com.sun.glass.events.KeyEvent;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

/**
 *
 * @author jesus
 */
public class CMDjesus extends javax.swing.JFrame {
Comandos comandos=new Comandos();
String x;
java.nio.file.Path ruta=Paths.get(System.getProperty("user.dir")); //esta sera la ruta determinada del programa

    public CMDjesus() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setTitle("CMD de Jesus Daniel"); 
        jTextArea1.append("Version de Jesus 1.0 .... \n para ver lista de comandos escriba:  help \n");
        jTextArea1.append(ruta+"\n");
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));

        jButton1.setText("Ejecutar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jScrollPane1.setAutoscrolls(true);

        jTextArea1.setEditable(false);
        jTextArea1.setBackground(new java.awt.Color(0, 0, 0));
        jTextArea1.setColumns(20);
        jTextArea1.setForeground(new java.awt.Color(255, 255, 255));
        jTextArea1.setLineWrap(true);
        jTextArea1.setRows(500);
        jTextArea1.setWrapStyleWord(true);
        jTextArea1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jScrollPane1.setViewportView(jTextArea1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 469, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 575, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 320, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
String texto=jTextField1.getText();  
String [] entrada=texto.split(" ");
if (null == texto){
    jTextArea1.append("Escriba un comando!! \n");  
 
}else switch (entrada[0]){
    
    //con el comando help leemos un TXT que hemos creado para dar la inf de uso de comandos 
    
        case "help":
            try{
                BufferedReader br = new BufferedReader(new FileReader("comandos.txt"));
                String linea, linea2 = new String();
                
                while((linea=br.readLine()) != null ){
                    System.out.println(linea);
                    jTextArea1.append(linea+"\n");
                }
                br.close();
                
            }catch (IOException ex){
                
            }   
            jTextField1.setText(null);
            break;
            
    /*cd mostrara la ruta en la que nos encontramos 
      cd (directorio) nos llevara a la ruta que le escribimos 
      cd .. no llevara a la ruta padre 
      cd (ruta) nos dira si la ruta en la que nos encontramos es absoluta*/   
            
        case "cd":
          
          if (entrada.length>1){
              
         if (entrada[1].contains("\\")||entrada[1].contains("/")){
             
              if(ruta.toString().equals(entrada[1])){
              File absoluta=new File (ruta.toString());
              if(absoluta.exists()){
                  jTextArea1.append("\n Es absoluta");
              }
              }else{
                  jTextArea1.append("\n NO es absoluta!!");
              }
        } else if ("..".equals(entrada[1])){
               
                if (ruta.endsWith(ruta.getRoot())){
               break;
           }else{
        
           jTextArea1.append("\n"+ruta.getParent()+"\n");
            ruta = ruta.getParent();
            
              }
        }else if (entrada.length>1&&entrada[1]!=".."){
              
        File fichero = new File(ruta.toString()+"\\"+entrada[1]);
        if (fichero.exists()){
         ruta=ruta.resolve(entrada[1]);
        }else{
          jTextArea1.append("\n no se puede acceder a una ruta que no existe \n");
              }
              
              if (ruta.isAbsolute()){
                  jTextArea1.append("\n"+ruta);
              }else{
                  jTextArea1.append("\n"+"la ruta no existe ");
              }
        }
         } else if (entrada.length==1){
               jTextArea1.append("\n"+ruta); 
          }
          
                
            break;
      //mkdir (directorio) nos creara un directorio en la ruta en la que estamos y si ya existe no lo creara
        case "mkdir":
       
{       if (entrada.length>1){
        File fichero = new File(ruta.toString()+"\\"+entrada[1]);
        try{
            
  
        if (fichero.exists()){
        jTextArea1.append("el directorio ya existe \n");
        }else{
            fichero.mkdir();
            jTextArea1.append("\n fichero creado como = "+entrada[1]);
        }
      }catch (SecurityException e) {
             jTextArea1.append("Error de seguridad");
}
}           break;
}
      
        case "info":
              /*<nombre>-> Muestra la información del elemento.
                Indicando FileSystem, Parent, Root, Nº of elements, FreeSpace, TotalSpace y UsableSpace.
                 y si es un fichero o un directorio */
            
      if (entrada.length>1){  
            File carpeta = new File(ruta.toString()+"\\"+entrada[1]);
         
            if (carpeta.exists()){
     try{
         if (entrada[1].contains(".")){
             jTextArea1.append("\n Ha solicitado usted la Info de un FICHERO: \n");
         }else{
             jTextArea1.append("\n \n ha solicitado usted la Info de un DIRECTORIO: \n");
         }
                long espacio=carpeta.getTotalSpace();
                long Free=carpeta.getFreeSpace();
                long Usable=carpeta.getUsableSpace();
          jTextArea1.append("\n \n FileSystem ="+ruta.getFileSystem().toString());
          jTextArea1.append("\n Parent ="+ruta.getParent().toString());
          jTextArea1.append("\n root ="+ruta.getRoot().toString());
          jTextArea1.append("\n Numero de elementos ="+ruta.getNameCount());
          jTextArea1.append("\n el Espacio total es de ="+espacio);
          jTextArea1.append("\n el Espacio libre total es de ="+Free);
          jTextArea1.append("\n el Espacio utilizable total es de ="+Usable);
          
     }catch (SecurityException e) {
             jTextArea1.append("Error de seguridad");
}    
          
           }else {
         jTextArea1.append("\n \n No hay ninguna carpeta de nombre "+entrada[1]+" en esta direccion \n");
         jTextArea1.append(" si desea la info de la carpeta en la que se encuentra haga un cd .. \n");
         jTextArea1.append(" o intenta dar doble espacio despues de info \n");
}
            }else {
               jTextArea1.append("\n No hay ninguna carpeta de nombre "+entrada[1]+" en esta direccion \n");
            }
      
           break;
     
            
        case "cat":
            // <nombreFichero>-> Muestra el contenido de un fichero. 
            
             if (entrada.length>1){  
            File carpeta = new File(ruta.toString()+"\\"+entrada[1]);
           
            if (carpeta.exists()){
                try{
           String[] listado = carpeta.list();
           if (listado == null || listado.length == 0) {
           jTextArea1.append("No hay elementos dentro de la carpeta actual");
    
           }else {
            jTextArea1.append("\nContenido del fichero ="+entrada[1]+"\n \n");
    for (int i=0; i< listado.length; i++) {
        jTextArea1.append("  "+listado[i].toString()+"\n");
    }
}
                }catch (SecurityException e) {
             jTextArea1.append("Error de seguridad");
}    
            }else {
               jTextArea1.append("No hay ninguna carpeta de nombre "+entrada[1]+" en esta direccion \n");
            }
}  
            break;
            
        case "top":
           //<numeroLineas> <nombreFichero>-> Muestra las líneas especificadas de un fichero. 
          
           int lineas = Integer.parseInt(entrada[1]);
           int ot=0;
           if (entrada.length>2){
               
           File fichero = new File(ruta.toString()+"\\"+entrada[2]);
           if (fichero.exists()){
          
         try{
             FileReader fr;
             BufferedReader br; 
             fr=new FileReader(entrada[2]);
             br=new BufferedReader(fr);
            
            String linea;
            int a=0;
             jTextArea1.append("\n Mostrando las primeras "+lineas+" lineas del fichero= "+entrada[2]+"\n \n");
            while ((linea=br.readLine())!=null && a!=lineas){

             if (a<lineas){
             String[] cont= new String[100];
             cont[a]=linea;
             jTextArea1.append(cont[a]+"\n"); 
             a++;
          
                  
             
             }else{
                 break;
             }
    
            }
          
            br.close();
            fr.close();
  
        }catch (java.io.IOException e){
            System.err.println("error: " + e);
        }catch (SecurityException e) {
             jTextArea1.append("Error de seguridad");
}        }else{
            jTextArea1.append("\n fichero no existe");
        }
             }
    
            
          break; 
          
        case "mkfile":
            //mkfile <nombreFichero> <texto>-> Crea un fichero con ese nombre y el contenido de texto.
           
           if (entrada.length>=2){
          File fichero = new File(ruta.toString()+"\\"+entrada[1]);
        if (fichero.exists()){
            jTextArea1.append("\n escribiendo en fichero existente");
        }else {
             jTextArea1.append("\n se ha creado un fichero con nombre = "+entrada[1]);
        }
            try{
            BufferedWriter bw = new BufferedWriter(new FileWriter(fichero, true));
            int num=2;
            bw.write("\n");
            while (entrada[num]!=null){
            bw.write(entrada[num]+" ");
            num++;
            bw.flush();
            
            }
           bw.close();
        }catch(IOException e){
            
        }catch (SecurityException e) {
             jTextArea1.append("Error de seguridad");
} 
           }   break;
           
           
        case "write":
            //write <nombreFichero> <texto>-> Añade 'texto' al final del fichero especificado.
           if (entrada.length>2){
          File fichero = new File(ruta.toString()+"\\"+entrada[1]);
        if (fichero.exists()){
            jTextArea1.append("\n escribiendo en fichero existente");
            try{
            BufferedWriter bw = new BufferedWriter(new FileWriter(fichero, true));
            int num=2;
            bw.write("\n");
            while (entrada[num]!=null){
            bw.write(entrada[num]+" ");
            num++;
            bw.flush();
            
            }
           bw.close();
        
        }catch(IOException e){
            
        } catch (SecurityException e) {
             jTextArea1.append("Error de seguridad");
} 
        }else {
             jTextArea1.append("\n el fichero no existe en este directorio");
        }
            
           }
            break;
            
        case "dir":
         /*-> Lista los archivos o directorios de la ruta actual.
               [<nombreDirectorio>]-> Lista los archivos o directorios dentro de ese directorio.
               [<rutaAbsoluta]-> Lista los archivos o directorios dentro de esa ruta.*/
            if (entrada.length==1){
                try{
            File dir = new File(ruta.toString());
            String[] ficheros=dir.list();
 
         if (ficheros == null|| ficheros.length == 0){
       System.out.println("No hay ficheros en el directorio especificado");
         }else { 
             jTextArea1.append("\nContenido del fichero actual =\n \n");
    for (int i=0; i< ficheros.length; i++) {
        jTextArea1.append("  "+ficheros[i].toString()+"\n");
    }
         }      
    
                }catch (SecurityException e) {
             jTextArea1.append("Error de seguridad");
}  
}else if (entrada.length>1){
    try{
    File carpeta = new File(ruta.toString()+"\\"+entrada[1]);
     String[] ficheros=carpeta.list();
     
      if (ficheros == null|| ficheros.length == 0){
       System.out.println("No hay ficheros en el directorio especificado");
         }else { 
             jTextArea1.append("\nContenido del fichero ="+entrada[1]+"\n \n");
    for (int i=0; i< ficheros.length; i++) {
        jTextArea1.append("  "+ficheros[i].toString()+"\n");
    }
            }
    }catch (SecurityException e) {
             jTextArea1.append("Error de seguridad");
}   
}
            break;
               
            
        case "delete":
            if (entrada.length>1){
                
        File fichero = new File(ruta.toString()+"\\"+entrada[1]);
        if (fichero.exists()){
            try{
        fichero.delete();
        if (entrada[1].contains(".")){
             jTextArea1.append("\n Ha borrado un FICHERO satisfactoriamente!! ");
        }else{
        jTextArea1.append("\n Ha Borrado un DIRECTORIO satisfactoriamente!! ");
        }
            }catch (SecurityException e) {
             jTextArea1.append("Error de seguridad");
}   
        }else{
            jTextArea1.append("\n el fichero o directorio( = "+entrada[1]+" ) no se encuentra!!");
        }
}  
            break;  
        case "close":
            comandos.close();
            break;
        case "clear":
            jTextArea1.setText(null);
            jTextArea1.append("Version de Jesus 1.0 .... \n para ver lista de comandos escriba:  help \n");
            jTextArea1.append(ruta+"\n");
            jTextField1.setText(null);
            
            break;
        default:
            jTextArea1.append("\n"+"error!! \n");
            jTextArea1.append("Version de Jesus 1.0 ....");
            break;
    }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode()==KeyEvent.VK_ENTER){
            jButton1.doClick();
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CMDjesus.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CMDjesus.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CMDjesus.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CMDjesus.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new CMDjesus().setVisible(true);
                
            }
        });
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}
